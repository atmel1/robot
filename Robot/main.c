/*
 * Robot.c
 *
 * Created: 4/14/2016 13:11:22
 * Author : Brandy
 */ 

#include <avr/io.h>
#include <avr/xmega.h>
//#include <avr/delay.h>

void setCPUClkto32Mwith16MCrystal(void);
void SquareWave(void); //pg.434
void two_toneSerin(void);

int k;

int main(void)
{
	setCPUClkto32Mwith16MCrystal();
	//GPIOA |= (<<)
    /* Replace with your application code */
	SquareWave();
    while (1) 
    {
		two_toneSerin();

    }
}

void setCPUClkto32Mwith16MCrystal(void)
{
	OSC.XOSCCTRL=0xCB;
	OSC.CTRL|=OSC_XOSCEN_bm;
	while (!(OSC.STATUS & OSC_XOSCRDY_bm));
	OSC.PLLCTRL=0xC2;
	OSC.CTRL|=OSC_PLLEN_bm;
	while(!(OSC.STATUS & OSC_PLLRDY_bm));
	CCP=0xD8;
	CLK.CTRL=0x04;
	CCP=0xD8;
	CLK.PSCTRL=0;
}

void SquareWave(void)
{
	PORTC.DIR=0x01;
	TCC0.CTRLB=0x11;
	TCC0.CCA=3999;
	TCC0.CTRLD=0;
	TCC0.CTRLA=0x03;
}

void two_toneSerin(void)
{
	PORTA.DIR|=0x80;
	for (k=0;k<125;k++)
	{
		PORTA.OUT|=0x80;
		//_delay_ms(2);
		PORTA.OUT&=0x7F;
		//_delay_ms(2);
	}
	for (k=0;k<225;k++)
	{
		PORTA.OUT|=0x80;
		//_delay_ms(2);
		PORTA.OUT&=0x7F;
		//_delay_ms(2);
	}
}